// back/src/index.js
import app from "./app";
/* import db from './db'
 */ import initializeDatabase from "./db";
import { hostname } from "os";
import {isLoggedIn } from "./auth";

const start = async () => {
  const controller = await initializeDatabase();
  app.get("/", async (req, res) => res.send("OK i am here"));

  //this is a test on createArticle;
 /*  const create_article = await controller.createArticle({
    title: "aaaaaaaaaaaaaaaaaaa ",
    text: "this is a test to see if the date will automatically be added",
  });
 */
  //this is a test on deleteArticle
  //const delete_article=await controller.deleteArticle(24);


 //this is a test to update and article
/*  const update_article= await controller.updateArticle({b:"i work"});
 */

//this is a test on getArticle
//const get_article = await controller.getArticle();

//this is a test on updateArticle
//const update_article=await controller.updateArticle(8,{title:"nMMMMm title",text:"yalla al batata",link:"www.zabatitelupdate.com"});

/*   app.get("/articles/list", async (req, res) => {
    const articles_list = await controller.getArticlesList("date");
    res.json(articles_list);
  }); */

  //CREATE 
//http://localhost:8080/articles/new?title=testingroutes&text=nshallatozbat
  app.get("/articles/new",async(req,res,next)=>{
try{
const {title,text,date,img_path,link}=req.query;
const result =await controller.createArticle({title,text,date,img_path,link});
res.json({success:true,result})
  }catch(e){
    next(e)
  }
  });

//READ
app.get("/articles/get/:id",async (req,res,next)=>{
  try{
const {id} =req.params;
const result=await controller.getArticle(id);
res.json({success:true,result})
//console.log(result);
}
catch(e){
  next(e)
}
})  

//UPDATE
app.get('/articles/update/:id',async(req,res,next)=>{
try{const {id}=req.params;
const {title,text,date,img_path,link}=req.query;
const result=await controller.updateArticle(id,{title,text,date,img_path,link});
res.json({success:true,result});
return result;}catch(e){
  next(e)
}
})


//LIST 
app.get('/articles/list',async(req,res,next)=>{
try{const {order}=req.query;
const articles=await controller.getArticlesList(order);
res.json({success:true,articles});
}catch(e){
  next(e)
}
})

// DELETE
app.get('/articles/delete/:id', async (req, res, next) => {
  try{
  const { id } = req.params
  const result = await controller.deleteArticle(id)
  res.json({success:true, result})}
  catch(e){next(e)}
})


//List of last 3 articles
app.get('/articles/recent',async(req,res,next)=>{
try{
  const article=await controller.getArticlesList("date");

const result = []; // 0 1 2
for (let index = 0; index < 3 && index< article.length; index++) {
  const element = article[index];
  result.push(element);
}
res.json(result);
//res.json({articles:[article[0],article[1],article[2]]});
}catch(e){
  next(e)
}
})

  

app.get('/mypage', isLoggedIn, ( req, res,next ) => {
  try{
  const username = req.user.name
  res.send({success:true, result: 'ok, user '+username+' has access to this page'})}
  catch(e)
  {
    next(e)
  }
})

//ERROR
app.use((err,req,res,next)=>{
console.error(err);
const message=err.message;
res.status(500).json({success:false,message})

})


  app.listen(8080, () => console.log("server listening on port 8080"));
};





start();
