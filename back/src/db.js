// back/src/db.js
import sqlite from "sqlite";
import SQL from "sql-template-strings";

const dateObj=new Date().toISOString().replace('-', '/').split('T')[0].replace('-', '/');
//console.log(dateObj);

const nowForSQLite = () => dateObj;

const joinSQLStatementKeys = (keys, values, delimiter , keyValueSeparator='=') => {
  return keys
    .map(propName => {
      const value = values[propName];
      if (value !== null && typeof value !== "undefined") {
        return SQL``.append(propName).append(keyValueSeparator).append(SQL`${value}`);
      }
      return false;
    })
    .filter(Boolean)
    .reduce((prev, curr) => prev.append(delimiter).append(curr));
};

const initializeDatabase = async () => {
  const db = await sqlite.open("./db.sqlite");

  // creates an article
  const createArticle = async props => {
    if (!props || !props.title || !props.text) {
      throw new Error(`you must provide a title a text and a date`);
    }

    const { title, text, img_path, link } = props;
    const date=nowForSQLite();
    try {
      const result = await db.run(
        SQL`INSERT INTO articles (title,text,img_path,date,link) VALUES (${title},${text},${img_path},${date},${link}) `
      );
      const id = result.stmt.lastID;
      return id;
    } catch (error) {
      throw new Error(`couldn't insert this combination` + error.message);
    }
  };

  //delete an article
  const deleteArticle = async id => {
    try {
      const result = await db.run(SQL`Delete FROM articles Where id=${id}`);
      if (result.stmt.changes === 0) {
        throw new Error(`article "${id}" doesn not exist`);
      }
      return true;
    } catch (error) {
      throw new Error(`couldn't delete the article "${id}": ` + error.message);
    }
  };

 /*  //Updating an article
  const updateArticle = async (id, props) => {
    if (
      !props ||
      !props.title ||
      !props.text ||
      !props.img_path ||
      !props.date ||
      !props.link
    ) {
      throw new Error`you need to give me something`();
    }

    const { title, text, img_path, date, link } = props;
    try {
      let statement = "";
      if (title && text && img_path && date && link) {
        statement = SQL`UPDATE articles SET title=${title},text=${text},img_path=${img_path},date=${date},link=${link}`;
      }
      const result = await db.run(statement);
      if (result.stmt.changes === 0) {
        throw new Error(`no changes where made`);
      }
      return true;
    } catch (error) {
      throw new Error(`couldn't update the article ${id} : ` + e.message);
    }
  }; */

  //Updating an article 2;
const updateArticle = async (id, props) => {
    const isValid = props && (
      props.title ||
      props.text ||
      props.img_path ||
      props.date ||
      props.link
    )
    if (!isValid) {
      throw new Error(`you need to give me something`);
    }
    try{
    const { title, text, img_path, date, link } = props;
    const statement = SQL`UPDATE articles SET `.append(
      joinSQLStatementKeys(
        ["title", "text", "img_path", "date", "link"],
        props,
        ", "
      )
    )
.append(SQL` WHERE id=${id}`)
const result=await db.run(statement);
if (result.stmt.changes === 0) {
  throw new Error(`no changes were made`);
}
const addedArticle = await getArticle(id);
return {status:true, updatedArticle: addedArticle};
} catch (e) {
throw new Error(`couldn't update the article ${id}: ` + e.message);
}


  };

  //Get article
  const getArticle = async id => {
    try {
      const articlesList = await db.all(
        SQL`SELECT id,title,text,img_path,date,link FROM articles WHERE id=${id}`
      );
      const article = articlesList[0];
      //console.log(article);

      if (!article) {
        throw new Error(`article ${id} not found`);
      }
      return article;
    } catch (error) {
      throw new Error(`couldn't get the article ${id}:` + error.message);
    }
  };

  // Get articles list with order by options

  const getArticlesList = async orderBy => {
    try {
      let statement = `SELECT id,title,text,date,img_path,link FROM articles `;
      switch (orderBy) {
        case "date":
          statement += `ORDER BY date DESC`;
          break;
        case "title":
          statement += `ORDER BY title`;
          break;
        default:
          break;
      }
      //console.log(statement)
      const rows = await db.all(statement);
      if (!rows.length) {
        throw new Error(`no rows found`);
      }
      return rows;
    } catch (error) {
      throw new Error(`couldn't retrieve articles:` + error.message);
    }
  };

  

  /*  const getArticlesList = async () => {
    const rows = await db.all(
      `SELECT id,title,text,img_path,date,link FROM articles`
    );
    return rows;
  };
 */

  const controller = {
    createArticle,
    getArticlesList,
    deleteArticle,
    updateArticle,
    getArticle
  };
  return controller;
};

export default initializeDatabase;
