import React, { Component } from "react";
import "./App.css";
import Article from "./Article";
import { pause, makeRequestUrl } from "./utils.js";
//import { Transition } from 'react-spring';
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { withRouter, Switch, Route, Link } from "react-router-dom";
import ArticleList from "./ArticleList";
import * as auth0Client from './auth';
const makeUrl = (path, params) =>
  makeRequestUrl(`http://localhost:8080/${path}`, params);

class App extends Component {
  state = {
    articles_list: [],
    error_message: "",
    title: "",
    text: "",
    date: "",
    link: "",
    img_path: "",
    isLoading: false,
    checkingSession:true
  };

  async componentDidMount() {
    this.getArticlesList() // <--- make sure this is at the top
    if (this.props.location.pathname === '/callback') {
      this.setState({checkingSession:false}); // <--- We Add this 
      return;
    }
    //this.logout();
    //this.login('chloe',123);
    // this.getArticle(49);
    //this.deleteArticle();
    /* this.createArticle({
      title: "yazid on chloe",
      text: "yazid is working on FE ,alaa on BA, mireille on FE",
      date: "2019/02/02",
      link: "chloe.com"
    }); */
    // this.updateArticle(49,{title:"update3",text:"trying to update the article2"})
    if (this.props.location.pathname === '/callback'){
      return
    }
    try {
      await auth0Client.silentAuth();
      await this.getPersonalPageData(); // get the data from our server
      this.forceUpdate();
    } catch (err) {
      if (err.error !== 'login_required'){
        console.log(err.error);
      }
      this.setState({checkingSession:false});
    }

  }
  /*  //GET ARTICLE LIST
  getArticlesList = async () => {
    try {
      const response = await fetch("//localhost:8080/articles/list");
      //in previous commits this answer was called data
      const answer = await response.json();
      console.log(answer);
      if (answer.success) {
        const articles_list = answer.articles;
        this.setState({ articles_list });
      } else {
        const error_message = answer.message;
        console.log(answer);
        this.setState({ error_message });
      }
    } catch (err) {
      this.setState({ error_message: err.message });
    }
  }; */

  //GET ARTICLE
  getArticle = async id => {
    const previous_article = this.state.articles_list.find(
      article => article.id === id
    );
    if (previous_article) {
      return "the article already exists";
    }
    try {
      const response = await fetch(`http://localhost:8080/articles/get/${id}`);
      const answer = await response.json();
      //console.log (answer)
      if (answer.success) {
        const article = answer.result;
        const articles_list = [...this.state.articles_list, article];
        this.setState({ articles_list });
      } else {
        this.setState({ error_message: answer.message });
      }
    } catch (err) {
      this.setState({ error_message: err.message });
    }
  };

  //DELETE ARTICLE
  deleteArticle = async id => {
    try {
      const response = await fetch(
        `http://localhost:8080/articles/delete/${id}`
      );
      const answer = await response.json();
      if (answer.success) {
        // remove the user from the current list of users
        const articles_list = this.state.articles_list.filter(
          article => article.id !== id
        );
        this.setState({ articles_list });
        toast("article deleted");
      } else {
        this.setState({ error_message: answer.message });
        toast.error(answer.message);
      }
    } catch (err) {
      this.setState({ error_message: err.message });
      toast.error(err.message);
    }
  };

  //CREATE ARTICLE
  createArticle = async props => {
    try {
      if (!props || !props.title || !props.text) {
        throw new Error(
          `you need at least a title and text to create an article`
        );
      }

      const { title, text, date, link, img_path } = props;
      const response = await fetch(
        `http://localhost:8080/articles/new?title=${title}&text=${text}&link=${link}&img_path=${img_path})`
      );
      const answer = await response.json();
      if (answer.success) {
        const id = answer.result;
        const article = { title, text, date, link, img_path, id };
        const articles_list = [...this.state.articles_list, article];
        this.setState({ articles_list });
      } else {
        this.setState({ error_message: answer.message });
      }
    } catch (err) {
      this.setState({ error_message: err.message });
    }
  };

  //GET ARTICLES LIST
  getArticlesList = async order => {
    this.setState({ isLoading: true });
    try {
      /*  const response = await fetch(
       `http://localhost:8080/articles/list?order=${order}`

      ); */
      const url = makeUrl(`articles/list`, { order});
      const response = await fetch(url);

      await pause();
      const answer = await response.json();
      if (answer.success) {
        const articles_list = answer.articles;
        this.setState({ articles_list: articles_list, isLoading: false });
        toast("articles loaded");
      } else {
        this.setState({ error_message: answer.message, isLoading: false });
        toast.error(answer.message);
      }
    } catch (err) {
      this.setState({ error_message: err.message, isLoading: false });
      toast.error(err.message);
    }
  };

  //UPDATE ARTICLES
  updateArticle = async (id, props) => {
    try {
      if (!props || !(props.title || props.text)) {
        throw new Error(
          `you need at least title or text properties to update an article`
        );
      }
      let url = "";
      const { title, text } = props;
      if (title && text) {
        url = `http://localhost:8080/articles/update/${id}?title=${title}&text=${text}`;
      } else if (title) {
        url = `http://localhost:8080/articles/update/${id}?title=${title}`;
      } else {
        url = `http://localhost:8080/articles/update/${id}?text=${text}`;
      }

      const response = await fetch(url);
      const answer = await response.json();
      if (answer.success) {
        // we update the user, to reproduce the database changes:
        const articles_list = this.state.articles_list.map(article => {
          // if this is the article we need to change, update it. This will apply to exactly
          // one article
          if (article.id === id) {
            const new_article = {
              id: article.id,
              title: props.title || article.title,
              text: props.text || article.text
            };
            toast("article updated");

            return new_article;
          }
          // otherwise, don't change the article at all
          else {
            return article;
          }
        });
        this.setState({ articles_list });
      } else {
        this.setState({ error_message: answer.message });
        toast.error(answer.message);
      }
    } catch (err) {
      this.setState({ error_message: err.message });
      toast.error(err.message);
    }
  };
  //EVENT HANDLERS
  onSubmit = evt => {
    console.log("heer");
    evt.preventDefault();
    const { title, text, date, img_path, link } = this.state;
    console.log(title);
    this.createArticle({ title, text, date, link, img_path });
    this.setState({ title: "", text: "", date: "", img_path: "", link: "" });
  };

  onTitleChange = evt => {
    const titleInput = evt.target;
    const title = titleInput.value;
    this.setState({ title });
  };

  onLinkChange = evt => {
    const linkInput = evt.target;
    const link = linkInput.value;
    this.setState({ link });
  };

  onTextChange = evt => {
    const textInput = evt.target;
    const text = textInput.value;
    this.setState({ text });
  };

  onImg_pathChange = evt => {
    const img_pathInput = evt.target;
    const img_path = img_pathInput.value;
    this.setState({ img_path });
  };

  onDateChange = evt => {
    const dateInput = evt.target;
    const date = dateInput.value;
    this.setState({ date });
  };

  renderHomePage = () => {
    const { articles_list } = this.state;
    return <ArticleList articles_list={articles_list} />;
  };

  renderArticlePage = props => {
    //console.log(props)
    const id = props.match.params.id;
    // eslint-disable-next-line eqeqeq
    const article = this.state.articles_list.find(
      article => article.id === parseInt(id)
    );
    if (!article) {
      return <div>{id} not found</div>;
    }
    return (
      <Article
        key={article.id}
        id={article.id}
        title={article.title}
        text={article.text}
        date={article.date}
        img_path={article.img_path}
        link={article.link}
        updateArticle={this.updateArticle}
        deleteArticle={this.deleteArticle}
      />
    );
  };

  renderProfilePage = () => {
    if(this.state.checkingSession){
      return <p>validating session...</p>
    }

    return (
      <div>
        <p>profile page</p>
        {this.renderUser()}
      </div>
    );
  };
  
  renderCreateForm = () => {
    return (
      <div>
        {" "}
        <form className="third" onSubmit={this.onSubmit}>
          <div>
            <input
              type="text"
              placeholder="title"
              onChange={evt => this.onTitleChange(evt)}
            />
          </div>

          <div>
            <textarea
              type="text"
              placeholder="text"
              onChange={evt => this.onTextChange(evt)}
            />
          </div>

          <div>
            <input
              type="date"
              placeholder="date"
              onChange={evt => this.onDateChange(evt)}
            />
          </div>
          <div>
            <input
              type="text"
              placeholder="link"
              onChange={evt => this.onLinkChange(evt)}
            />
          </div>
          <div>
            <input
              type="text"
              placeholder="img_path"
              onChange={evt => this.onImg_pathChange(evt)}
            />
          </div>
          <div>
            <input type="submit" value="OK" />
            <input type="reset" value="Cancel" className="button" />
          </div>
        </form>
      </div>
    );
  };

  renderContent() {
    if (this.state.isLoading) {
      return <p>loading...</p>;
    }
    return (
      <Switch>
        <Route path="/" exact render={this.renderHomePage} />
        <Route path="/article/:id" render={this.renderArticlePage} />
        <Route path="/profile" render={this.renderProfilePage} />
        <Route path="/create" render={this.renderCreateForm} />
        <Route path="/callback" render={this.handleAuthentication} />
        <Route render={() => <div>not found!</div>} />
       

      </Switch>
    );
  }

  isLogging = false;
  login = async () => {
    console.log("hello")
    if (this.isLogging === true) {
      return;
    }
    this.isLogging = true;
    try{
      await auth0Client.handleAuthentication();
      const name = auth0Client.getProfile().name // get the data from Auth0
      console.log(name)
      console.log(auth0Client.getProfile())
      await this.getPersonalPageData() // get the data from our server
      toast(`${name} is logged in`)
      this.props.history.push('/profile')
    }catch(err){
      this.isLogging = false
      toast.error(err.message);
    }
  }
  handleAuthentication = ({history}) => {
    this.login(history)
    return <p>wait...</p>
  }




  renderUser() {
    const isLoggedIn = auth0Client.isAuthenticated()
    if (isLoggedIn) {
      // user is logged in
      return this.renderUserLoggedIn();
    } else {
      return this.renderUserLoggedOut();
    }
  }
  renderUserLoggedOut() {
    return (
      <button onClick={auth0Client.signIn}>Sign In</button>
    );
  }
  renderUserLoggedIn() {
    const nick = auth0Client.getProfile().name
    return <div>
Hello, {nick}! <button onClick={()=>{auth0Client.signOut();this.setState({})}}>logout</button>
    </div>
  }


getPersonalPageData=async()=>{
try{
const url=makeUrl(`mypage`);
const response = await fetch(url,{ headers: { 'Authorization': `Bearer ${auth0Client.getIdToken()}` } });
const answer=await response.json();
if(answer.success){
  const message=answer.result
   // we should see: "received from the server: 'ok, user <username> has access to this page'"
  toast(`received from the server: '${message}'`);
}
else{
  this.setState({error_message:answer.message});
  toast.error(answer.message);
}


}
catch(err){
  this.setState({ error_message: err.message });
  toast.error(err.message);
}



}


  render() {
    return (
      <div className="App">
        <div>
          <Link to="/">Home</Link> |<Link to="/profile">profile</Link> |
          <Link to="/create">create</Link>
        </div>
        {this.renderContent()}
        <ToastContainer />
        <button onClick={this.getPersonalPageData}>get personal page data</button>

      </div>
    );
  }
}

export default withRouter(App);
