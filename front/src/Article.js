import React from "react";
export default class Article extends React.Component {
  state = {
    editMode: false
  }

  toggleEditMode = () => {
    const editMode = !this.state.editMode
    this.setState({ editMode })
  }

  renderEditMode() {
    const {  title, text, date, link, img_path } = this.props;
    return (
      <form className="third" onSubmit={this.onSubmit} onReset={this.toggleEditMode}>
        <input
          type="text"
          placeholder="title"
          name="article_title_input"
          defaultValue={title}
        >
        </input>

        <input
          type="text"
          placeholder="text"
          name="article_text_input"
          defaultValue={text}
        >
        </input>

        <input
          type="text"
          placeholder="date"
          name="article_date_input"
          defaultValue={date}
        >
        </input>


        <input
          type="text"
          placeholder="img_path"
          name="article_img_path_input"
          defaultValue={img_path}
        >
        </input>


        <input
          type="text"
          placeholder="link"
          name="article_link_input"
          defaultValue={link}
        >
        </input>
        <div>
          <input type="submit" value="OK"/>
          <input type="reset" value="cancel" className="button"/>
        </div>

      </form>

    )
  }

  onSubmit = (evt) => {

    evt.preventDefault();
    const form = evt.target;
    const article_title_input = form.article_title_input;
    const article_text_input = form.article_text_input;
    const article_date_input = form.article_date_input;
    const article_img_path_input = form.article_img_path_input;
   const article_link_input = form.article_link_input;

    const title = article_title_input.value
    const text = article_text_input.value
    const date = article_date_input.value
    const link = article_link_input.value
    const img_path = article_img_path_input.value


    const { id,updateArticle } = this.props;
    updateArticle(id,{ title, text,date,link,img_path });
    this.toggleEditMode();

  }





  renderViewMode() {
    const { id, title, text, date, link, img_path} = this.props;
    return (
      <div>
        <span>
          {id} - {title} - {text} - {date}  - {link}  - {img_path}
        </span>
        <button onClick={() => this.props.deleteArticle(id)} className="warning">x</button>
        <button className="success" onClick={this.toggleEditMode}>edit</button>

      </div>
    )
  }


  render() {
    const { editMode } = this.state;
    if (editMode) {
      return this.renderEditMode()

    }

    else { return this.renderViewMode() }


















  }

}
