// front/src/utils.js
export const sleep = ms => new Promise(ok => setTimeout(ok, ms));
export const getRandomInteger = (max, min = 0) =>
  Math.floor(Math.random() * (max - min + 1)) + min;
export const pause = () => sleep(getRandomInteger(1000, 4000));

export const objectToQuery = params => {
  return Object.keys(params)
    .filter(key => params[key] !== undefined)
    .map(key => encodeURIComponent(key) + "=" + encodeURIComponent(params[key]))
    .join("&")
    .trim();
};

export const makeRequestUrl = (path, params) => {
  if(!params){ return path } // if no parameters were provided, return early
  const query = objectToQuery(params)
  if(query.length){
    const has_interrogation_mark = path.indexOf('?') >= 0 
    const url = path + (has_interrogation_mark ? '&' : '?') + query;
    return url
  }
  return path
}
